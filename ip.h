int IsValidIp(char* szIP) ;

void extraireChamps(char* ip, char* masque);

void convertirChamps(char* ip1,char* masque,int* ip,int* masq) ;

void decoderIP (int* ip, char* classe, char* type) ;

void calcAdresse (int* ip, int masq,int* reseau) ;

void conversionBD (int nombre, int* bin) ;

void analyseIP(char* Ip);