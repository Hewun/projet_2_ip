#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "ip.h"

int IsValidIp(char* szIP)
{
    int isValid;
    int i;
    int byte;
    int iIpLen;
    char dotCount;
	int SCount = 0 ;	
       
       
    isValid = 1;
    iIpLen = strlen(szIP);
    dotCount = 0;
   
    if(iIpLen == 0 || iIpLen > 18)
    {
        isValid = 0;
    }
    else
    {
        byte = atoi(szIP);
        if(byte > 255 || byte < 0)
        {
            isValid = 0;
        }
           
        for(i = 0; i < iIpLen && isValid == 1; i++)
        {
            if(!(szIP[i] >= '0' && szIP[i] <= '9') && szIP[i] != '.' && szIP[i] != '/')
            {
                isValid = 0;
            }
            if(szIP[i] == '.')
            {
                dotCount ++;
                byte = atoi(&szIP[i+1]);
                if(byte > 255 || byte < 0)
                {
                    isValid = 0;
                }
            }
			if(szIP[i] == '/')
			{
                SCount ++;
                byte = atoi(&szIP[i+1]);
                if(byte > 32 || byte < 0)
                {
                    isValid = 0;
                }
            }
        }
        if(dotCount != 3)
        {
            isValid = 0;
        }
		if(SCount != 1)
        {
            isValid = 0;
        }         
    }
    return isValid;
}

void extraireChamps(char* ip, char* masque){
	char* p ;
	p = strchr(ip,'/') ;
	strcpy(masque,p+1) ;
	strtok(ip,"/") ;
}

void convertirChamps(char* ip1,char* masque,int* ip,int* masq) {
	int p = atoi(masque) ;
	*masq = p ;
	int byte ;
	byte = atoi(ip1) ;
	ip[0] = byte ;
	int compt = 0 ;
    for(int i = 0; i < 15 ; i++)
    {
        if(ip1[i] == '.')
        {
            compt ++;
            byte = atoi(&ip1[i+1]);
			ip[compt] = byte ;
   		}
	}
}

void decoderIP (int* ip, char* classe, char* type) {
	if (ip[0] < 128) 
	{
		*classe = 'A' ;
		if (ip[0] == 0 || ip[0] == 10) 
		{
			strcpy(type,"privee") ;
		} 
		else if (ip[0] == 127) 
		{
			strcpy(type,"localhost") ;
		}
		else 
		{
			strcpy(type,"publique") ;
		}
	}
	else if (ip[0] < 192) 
	{
		*classe = 'B' ;
		if (ip[0] == 172 && ip[1] < 32 && ip[1] > 15) 
		{
			strcpy(type,"privee") ;
		} 
		else
		{
			strcpy(type,"publique") ;
		}
	}
	else if (ip[0] < 224) 
	{
		*classe = 'C' ;
		if (ip[0] == 192 && ip[1] == 168) 
		{
			strcpy(type,"privee") ;
		} 
		else
		{
			strcpy(type,"publique") ;
		}
	}
	else if (ip[0] < 240) 
	{
		*classe = 'D' ;
		strcpy(type,"multicast") ;
	}
	else if (ip[0] >= 240) 
	{
		*classe = 'E' ;
		strcpy(type,"reservee") ;
	}
	if (*classe == 'A' && ip[1] == ip [2] == ip [3] == 255) 
	{
		strcpy(type,"broadcast") ;
	}
		if (*classe == 'B' && ip [2] == ip [3] == 255) 
	{
		strcpy(type,"broadcast") ;
	}
		if (*classe == 'C' && ip [3] == 255) 
	{
		strcpy(type,"broadcast") ;
	}	
}

void calcAdresse (int* ip, int masq,int* reseau) {
	int i = masq/8 ;
	int bin[8] ;
	for (int a=0;a<i;a++) {
		reseau[a] = ip[a] ;
	}
	for (int c=3;c>i;c--) {
		reseau[c] = 0 ;
	}
	if (i != 4) {
		conversionBD(ip[i],bin) ;
		int b = masq%8 ;
		for (int b = masq%8; b<7;b++) {
			bin[b] = 0 ;
		}
		int v = 0;
		int h = 7 ;
		for (int j =0; j < 7; j++) {
			if (bin[j] == 1) {
				v += pow(2,h) ;
			}
			h = h-1 ;
		}
		reseau[i] = v ;
	}
}

void conversionBD (int nombre, int* bin) {
    int cnt,i;  
	int b = 7 ;
	for (int i=0;i<b;i++) {
		bin[i] = 0 ;
	}
    while(nombre>0)
    {
        bin[b]=nombre%2;
        nombre /=2;
        b--;
    }
}

void analyseIP(char* Ip){
	char ipc[30] ;
	strcpy(ipc,Ip) ;
	char masque[10] ;
	int ip[4] ;
	int masq ;
	char classe ;
	char type[30] ;
	int reseau[4] ;	
	
	if (IsValidIp(Ip)==1) {
		extraireChamps(ipc,masque) ;
		convertirChamps(ipc,masque,ip,&masq) ;
		decoderIP(ip,&classe,type) ;
		calcAdresse(ip,masq,reseau) ;

		FILE* fic; 
		fic = fopen("IpResults","w");


		if (fic == NULL){
			printf("Erreur à l'écriture du fichier : IpResults\n");

		}
		else{

		fprintf(fic, "Adresse ip : %s\n", ipc);

		fprintf(fic, "    Masque : %s\n", masque);

		fprintf(fic, "    Classe : %c\n", classe);

		fprintf(fic, "      Type : %s\n", type);

		fprintf(fic, "   @Reseau : ");
		for (int i = 0;i<4;i++) {
			fprintf(fic,"%d",reseau[i]) ;
			if (i<3) {
				fprintf(fic,".") ;
			}
		}

		fclose(fic);

		}
	} else {
		printf("Erreur adresse invalide\n") ;
	}

}