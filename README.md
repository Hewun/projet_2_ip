# Projet M2101 Sujet 2 : Analyse adresse IP

* [Framagit](https://framagit.org/Hewun/projet_2_ip)

1ère année DUT Informatique, Projet de M2101 portant sur l'analyse d'adresse IP en utilisant le langage C.<br/>
Ce projet a été réalisé par deux étudiants Peter Piriou--Dezy et Billal Hanache.

Pour ce faire nous avons suivi le schéma de pensée donné : 

![alt text](https://i.ibb.co/1RfrYsv/Projet-M2101.png)


## Ressources

* https://fr.wikipedia.org/wiki/Adresse_IP
* https://fr.wikipedia.org/wiki/Classe_d'adresse_IP
* https://fr.wikipedia.org/wiki/Classe_d'adresse_IP#R%C3%A9sum%C3%A9
* https://fr.wikipedia.org/wiki/Adresse_IP#Plages_d'adresses_IP_sp%C3%A9ciales
* https://fr.wikipedia.org/wiki/Broadcast_(informatique)


# Documentation


## Fonctions & Méthodes

|Sommaire                                                                                    |
| ------                                                                                                   |
|   [IsValidIp](#isvalidip) - Vérifie que l'IP entrée par l'utilisateur est valide au format CIDR                                   |
|   [extraireChamps](#extrairechamps) - Sépare l'IP et le masque                                         |
|   [convertirChamps](#convertirchamps) - Convertit le format de l'ip et du masque de base en chaine de caractère en entiers                     |
|   [decoderIP](#decoderip) - Décode l'IP afin de nous donner son type et sa classe                    |
|   [calcAdresse](#calcadresse) - Calcule l'adresse réseau            |
|   [conversionBD](#conversionbd) - Convertit un entier en binaire                                  |
|   [analyseIP](#analyseip) - Analyse l'IP à l'aide des méthodes précédentes et écrit les résultat dans un fichier texte                         |


### IsValidIp

```c
int isvalidip(char* szIP);
```
Vérifie la longueur ainsi que la contenance de la chaine de caractères en entrée.<br/>
Retourne 1 si la chaine de caractère est valide, sinon 0.

Paramètres :

> * char* szIP  - Adresse à verifier

Return :

> * int - Si la chaine est valide affiche 1 sinon 0.


### extraireChamps

```c
void extrairechamps(char* ip, char* masque);
```

Sépare la chaine de caractère contenu dans ip en deux chaines de caractères.
Ip ne contenant que ce qu'il ya avant le '/' et masque contenant ce qu'il y a après.

Paramètres :

> * char* ip - Contient adresse valide
>
> * char* masque - Chaine de caractères vide visant à contenir la notation CIDR


### convertirChamps

```c
void convertirchamps(char* ip1,char* masque,int* ip,int* masq);
```

Stocke ce qui est contenu dans les chaines de caractères entrantes dans les tableaux d'entiers entrants
ip1 -> ip et masque -> masq

Paramètres :

> * char* ip - Contient l'adresse ip
>
> * char* masque - Contient la notation CIDR
>
> * int* ip - Tableau d'entiers vide visant à contenir l'ip
>
>* int* masq - Tableau d'entiers vide visant à contenir le masque


### decoderIP

```c
void decoderip (int* ip, char* classe, char* type);
```

Donne et stocke la valeur de la classe et du type dans les chaines de caractères entrée <br/>
en fonction de la valeur des octets stockés dans le tableau d'entiers en entrée

Paramètres :

> * int* ip - Tableaux d'entiers contenant l'adresse ip (1 case = 1 octet)
>
> * char* classe - Chaine de caractères vide visant à contenir la valeur de la classe
>
> * char* type - Chaine de caractères vide visant à contenir la valeur du type


### calcAdresse

```c
void calcadresse (int* ip, int masq,int* reseau);
```

Calcule l'adresse réseau à partir de ip et de masq et la stocke dans reseau

Paramètres :

> * int* ip - Contient l'adresse ip
>
> * int masq - Contient le masque
>
> * int* reseau - Tableau d'entiers vide visant à contenir l'adresse réseau


### conversionBD

```c
void conversionbd (int nombre, int* bin);
```

Stocke la valeur binaire du premier paramètre (nombre) dans le deuxième paramètre (bin)

Paramètres :

> * int nombre - Entier à convertir en binaire
>
> * int* bin - Tableau vide visant à stocker la valeur binaire du premier paramètre


### analyseIP

```c
void analyseip(char* Ip);
```

Utilise toutes les méthodes précédentes et écrit les résultats dans le fichier texte "IpResults"

Paramètres :

> * char* Ip - Adresse IP à analyser

